#!/bin/bash

# Verify arguments amount
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 22
fi

current_time=$(date)
args=("$@")
ips_list=${args[0]}

# Ping all given addresses
cat $ips_list |  while read output
do
    ping -c 1 "$output" > /dev/null
    if [ $? -eq 0 ]; then
    echo -e "$current_time:\t$output is up" 
    else
    echo -e "$current_time:\t$output is down"
    fi
done
