# write out current crontab
crontab -l > mycron
# add the new cron into cron file
echo "* * * * * $(pwd)/curler.sh  >> $(pwd)/curler_log.txt" >> mycron
# install new cron file
crontab mycron
# check return code
if [ $? -eq 0 ]
then
    echo "Successfully add cron! crontab -l:"
else
    echo "Could not add cron" >&2
fi
rm mycron

# set logratate
echo "Setting logrotate for curler_log.txt..."
config="$(pwd)/curler_log.txt {
        size=20M
        create 777 iguazio
        rotate 4
}"
echo "$config"  | sudo tee /etc/logrotate.d/underground-network-monitor

# check return code
if [ $? -eq 0 ] 
then
    echo "Successfully add logrotate config."
else
    echo "Could not add logrotate config." >&2
fi

# check config
if grep -q "$config" /etc/logrotate.d/underground-network-monitor
then
    exit 0
else
    echo "Something went wrong, and you didn't add to /etc/logrotate.d/ the expected config"
    exit 1
fi
