#!/usr/bin/env bash

# Elasticsearch IP + PORT
URL=192.168.40.83
PORT=9200

# Save the machine name + timestamp
TYPE=es_test
BUILDER=$(ifconfig eth0 | grep "inet " | awk -F'[: ]+' '{ print $4 }')
NOW=$(date +"%Y-%m-%dT%H:%M:%S")

curl_output=$(curl -s -i -H "Content-Type:application/json" \
              -XPOST "http://$URL:$PORT/naipi/$TYPE/" \
              --data "{\"id\": \""$NOW"\", \"builder\": \""$BUILDER"\"}")
output="$NOW\n"
output+=$curl_output"\n"
echo -e $output
