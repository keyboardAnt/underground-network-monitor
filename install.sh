# write out current crontab
crontab -l > mycron
# add the new cron into cron file
echo "* * * * * $(pwd)/pingall.sh $(pwd)/ips.list >> $(pwd)/log.txt" >> mycron
# install new cron file
crontab mycron
# check return code
if [ $? -eq 0 ]
then
    echo "Successfully add cron! crontab -l:"
else
    echo "Could not add cron" >&2
fi
rm mycron
# info message about crontab, and target addresses
echo "$(crontab -l)"
echo -e '\n'
echo "You can change cron's ping target addresses by edit 'ips.list' file. Current 'ips.list' content:"
echo "$(cat ips.list)"
echo -e '\n'

# set logratate
echo "Setting logrotate for log.txt..."
config="$(pwd)/log.txt {
        size=350k
        create 777 iguazio
        rotate 4
}"
echo "$config"  | sudo tee /etc/logrotate.d/underground-network-monitor

# check return code
if [ $? -eq 0 ] 
then
    echo "Successfully add logrotate config."
else
    echo "Could not add logrotate config." >&2
fi

# check config
if grep -q "$config" /etc/logrotate.d/underground-network-monitor
then
    exit 0
else
    echo "Something went wrong, and you didn't add to /etc/logrotate.d/ the expected config"
    exit 1
fi
